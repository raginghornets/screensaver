import java.util.ArrayList;
import processing.core.*;

/** <h1>Name: ScreensaverMain.java</h1>
 * <h2>Author: Eric Nguyen</h2>
 * <h2>Description:</h2>
 * <li>Screensaver with movable players
 * <li>Date: September 24, 2017
 * <li>Teacher: Mr. Sharick
 * <h2>Notes:</h2>
 * <li>I DREW PICTURE OF DOGE :D
 * <li>The framerate depends on location of the player
 * <h2>Controls: >> MAKE SURE CAPS LOCK IS OFF << </h2>
 * <li>1 - select triangle
 * <li>2 - select doge
 * <li>w - Move player up
 * <li>a - Move player left
 * <li>s - Move player down
 * <li>d - Move player right
 * <li>f - hide player (ONLY WORKS ON NON-IMAGE PLAYERS)
 * <li>r - reset player
 * <li>q - increase player size
 * <li>e - decrease player size
 * <li>Left Mouse Button - fire projectile
 * <li>Right Mouse Button - set player position to mouse position
 **/
public class Screensaver extends PApplet{
	// Window dimensions
	int winX = 1000;
	int winY = 1000;
	
	// Colors
	Color color1 = new Color(255, 127, 0, 1);
	Color color2 = new Color(100, 125, 250, 1);
	
	// Player
	TrianglePlayer trianglePlayer = new TrianglePlayer(winX/2+50, winY/2, 50, 25);
	ImagePlayer imagePlayer = new ImagePlayer(winX/2-25, winY/2, 50, 25);
	
	// >> THE STARTING PLAYER <<
	Player player = trianglePlayer;
	Player otherPlayer = imagePlayer;
	Projectile projectile = new Projectile(0, 0);
	
	// Ellipses
	int initNumCircles = 19;
	ArrayList<Ellipse> circles1 = new ArrayList<Ellipse>(initNumCircles);
	ArrayList<Ellipse> circles2 = new ArrayList<Ellipse>(initNumCircles);
	
	Ellipse circle1 = new Ellipse(0,0,50,0,-3,5,5);
	Ellipse circle2 = new Ellipse(0,0,50,3,0,5,5);
	
	
	ArrayList<Ellipse> circles3 = new ArrayList<Ellipse>(initNumCircles);
	ArrayList<Ellipse> circles4 = new ArrayList<Ellipse>(initNumCircles);
	Ellipse circle3 = new Ellipse(0,0,50,0,3,5,5);
	Ellipse circle4 = new Ellipse(0,0,50,-3,0,5,5);
	
	// Loop
	ArrayList<Ellipse> loopCircles1 = new ArrayList<Ellipse>(initNumCircles);
	Ellipse loopCircle1 = new Ellipse(-25,-25,50,1,0,5,5);
	
	// Rectangles
	int initNumSquares = 18;
	ArrayList<Rectangle> squares1 = new ArrayList<Rectangle>(initNumSquares);
	ArrayList<Rectangle> squares2 = new ArrayList<Rectangle>(initNumSquares);
	
	Rectangle square1 = new Rectangle(0, 0, 50, 0, 3, 5, 5);
	Rectangle square2 = new Rectangle(0, 0, 50, -3, 0, 5, 5);
	
	
	ArrayList<Rectangle> squares3 = new ArrayList<Rectangle>(initNumSquares);
	ArrayList<Rectangle> squares4 = new ArrayList<Rectangle>(initNumSquares);
	Rectangle square3 = new Rectangle(0, 0, 50, 0, -3, 5, 5);
	Rectangle square4 = new Rectangle(0, 0, 50, 3, 0, 5, 5);
	
	// Loop
	Rectangle loopSquare1 = new Rectangle(-50,-50,50,1,0,10,10);
	ArrayList<Rectangle> loopSquares1 = new ArrayList<Rectangle>(initNumSquares);
	
	boolean showBackground = false;
	
	public static void main(String[] args) {
		PApplet.main("Screensaver");
	}
	
	public void settings()
	{
		size(winX, winY);
	}
	
	public void setup()
	{
		background(color1.red, color1.green, color1.blue);
//		strokeWeight(5);
		square1.setupSquares(squares1, initNumSquares);
		square2.setupSquares(squares2, initNumSquares);
		square3.setupSquares(squares3, initNumSquares);
		square4.setupSquares(squares4, initNumSquares);
		circle1.setupCircles(circles1, initNumCircles);
		circle2.setupCircles(circles2, initNumCircles);
		circle3.setupCircles(circles1, initNumCircles);
		circle4.setupCircles(circles2, initNumCircles);
		loopCircle1.setupCircles(loopCircles1, 10);
		loopSquare1.setupSquares(loopSquares1, 10);
		imagePlayer.setup();
		trianglePlayer.setup();
	}
	
	public void draw()
	{
		frameRate(Math.abs(player.x+player.y)/10);
		color1.animateFill();
		noStroke();
		if(showBackground)
		{
			color2.animateAltBackground();
		}
		square1.moveSquares();
		square2.moveSquares();
		square3.moveSquares();
		square4.moveSquares();
		loopSquare1.squareLoop();
		loopCircle1.squareLoop();
		color1.animateAltFill();
		circle1.moveCircles();
		circle2.moveCircles();
		circle3.moveCircles();
		circle4.moveCircles();
		spawnPlayers();
		if(projectile.active)
		{
			projectile.fireCircleProjectile();
		}
		println(player.collider.collidingWithPlayerRight(otherPlayer));
	}
	
	public void spawnPlayers()
	{
		player.collider = new Collider2D(player.x, player.y, player.size);
		if(imagePlayer.selected)
		{
			trianglePlayer.spawn();
			imagePlayer.spawn();
		}
		else if(trianglePlayer.selected)
		{
			imagePlayer.spawn();
			trianglePlayer.spawn();
		}
		player.showText(trianglePlayer, imagePlayer);
	}
	
	public void keyPressed()
	{
		if(key == 'w' && !player.collider.collidingTop() && !player.collider.collidingWithPlayerBottom(otherPlayer))
		{
			player.moveUp();
		}
		else if(key == 'w' && player.collider.collidingTop())
		{
			player.ceiling();
		}
		else if(key == 'w' && player.collider.collidingWithPlayerBottom(otherPlayer))
		{
			player.faceUp();
		}
		if(key == 's' && !player.collider.collidingBottom() && !player.collider.collidingWithPlayerTop(otherPlayer))
		{
			player.moveDown();
		}
		else if(key == 's' && player.collider.collidingBottom())
		{
			player.floor();
		}
		else if(key == 's' && player.collider.collidingWithPlayerTop(otherPlayer))
		{
			player.faceDown();
		}
		if(key == 'a' && !player.collider.collidingLeft() && !player.collider.collidingWithPlayerRight(otherPlayer))
		{
			player.moveLeft();
		}
		else if(key == 'a' && player.collider.collidingLeft())
		{
			player.leftWall();
		}
		else if(key == 'a' && player.collider.collidingWithPlayerRight(otherPlayer))
		{
			player.faceLeft();
		}
		if(key == 'd' && !player.collider.collidingRight() && !player.collider.collidingWithPlayerLeft(otherPlayer))
		{
			player.moveRight();
		}
		else if(key == 'd' && player.collider.collidingRight())
		{
			player.rightWall();
		}
		else if(key == 'd' && player.collider.collidingWithPlayerLeft(otherPlayer))
		{
			player.faceRight();
		}
		if(key == 'f' && player.visible())
		{
			player.hide();
		}
		else if(key == 'f' && !player.visible())
		{
			player.show();
		}
		if(key == 'r')
		{
			player.reset();
		}
		if(key == 'q' && !player.collider.colliding() && !player.collider.collidingWithPlayer(otherPlayer))
		{
			player.size *= 2;
		}
		if(key == 'e')
		{
			player.size /= 2;
		}
		if(key == '1' && player != trianglePlayer)
		{
			trianglePlayer.select();
			imagePlayer.deselect();
		}
		else if(key == '2' && player != imagePlayer)
		{
			imagePlayer.select();
			trianglePlayer.deselect();
		}
		if(key == 'b' && !showBackground)
		{
			showBackground = true;
		}
		else if(key == 'b' && showBackground)
		{
			showBackground = false;
		}
		if(key == CODED)
		{
			if(keyCode == UP)
			{
				player.faceUp();
			}
			if(keyCode == DOWN)
			{
				player.faceDown();
			}
			if(keyCode == LEFT)
			{
				player.faceLeft();
			}
			if(keyCode == RIGHT)
			{
				player.faceRight();
			}
		}
	}
	
	public void mousePressed()
	{
		if(mouseButton == LEFT)
		{
			projectile = new Projectile(20, 10);
			projectile.setDirection();
			projectile.x = player.x;
			projectile.y = player.y;
			projectile.active = true;
		}
		if(mouseButton == RIGHT)
		{
			player.setPosition(mouseX, mouseY);
		}
	}
	
	class Collider2D
	{
		float x, y, size;
		
		Collider2D(float x, float y, float size)
		{
			this.x = x;
			this.y = y;
			this.size = size;
		}
		
		/**
		 * @return true, if colliding with top border
		 */
		public boolean collidingTop()
		{
			return y <= size/2;
		}
		
		/**
		 * @return true, if colliding with bottom border
		 */
		public boolean collidingBottom()
		{
			return y >= winY - size/2;
		}
		
		/**
		 * @return true, if colliding with left border
		 */
		public boolean collidingLeft()
		{
			return x <= size/2;
		}
		
		/**
		 * @return true, if colliding with right border
		 */
		public boolean collidingRight()
		{
			return x >= winX - size/2;
		}
		
		/**
		 * @return true, if colliding with any border
		 */
		public boolean colliding()
		{
			return collidingBottom() || collidingLeft() || collidingRight() || collidingTop();
		}
		
		/**
		 * @param col player to collide with
		 * @return true, if this is colliding with col's bottom side
		 */
		public boolean collidingWithPlayerBottom(Player col)
		{
			if(this.size > col.size)
			{
				return this.x >= col.x - this.size/2 && this.x <= col.x + this.size/2 && (col.y + col.size/2 >= this.y - this.size/2 && col.y + col.size/2 <= this.y);
			}
			else
			{
				return this.x >= col.x - col.size/2 - this.size/4 && this.x <= col.x + col.size/2 + this.size/4 && (col.y + col.size/2 >= this.y - this.size/2 && col.y + col.size/2 <= this.y);
			}
		}
		
		/**
		 * @param col player to collide with
		 * @return true, if this is colliding with col's top side
		 */
		public boolean collidingWithPlayerTop(Player col)
		{
			if(this.size > col.size)
			{
				return this.x >= col.x - this.size/2 && this.x <= col.x + this.size/2 && (col.y - col.size/2 >= this.y && col.y - col.size/2 <= this.y + this.size/2);
			}
			else
			{
				return this.x >= col.x - col.size/2 - this.size/4 && this.x <= col.x + col.size/2 + this.size/4 && (col.y - col.size/2 >= this.y && col.y - col.size/2 <= this.y + this.size/2);
			}
		}
		
		/**
		 * @param col player to collide with
		 * @return true, if this is colliding with col's left side
		 */
		public boolean collidingWithPlayerLeft(Player col)
		{
			if(this.size > col.size)
			{
				return (col.x - col.size/2 >= this.x && col.x - col.size/2 <= this.x + this.size/2) && this.y >= col.y - this.size/2 && this.y <= col.y + this.size/2;
			}
			else
			{
				return (col.x - col.size/2 >= this.x && col.x - col.size/2 <= this.x + this.size/2) && this.y >= col.y - col.size/2 - this.size/4 && this.y <= col.y + col.size/2 + this.size/4;
			}
		}
		
		/**
		 * @param col player to collide with
		 * @return true, if this is colliding with col's right side
		 */
		public boolean collidingWithPlayerRight(Player col)
		{
			if(this.size > col.size)
			{
				return (col.x + col.size/2 >= this.x - this.size/2 && col.x + col.size/2 <= this.x) && this.y >= col.y - this.size/2 && this.y <= col.y + this.size/2;
			}
			else
			{
				return (col.x + col.size/2 >= this.x - this.size/2 && col.x + col.size/2 <= this.x) && this.y >= col.y - col.size/2 - this.size/4 && this.y <= col.y + col.size/2 + this.size/4;
			}
		}
		
		/**
		 * @param col player to collide with
		 * @return true, if this is colliding with col
		 */
		public boolean collidingWithPlayer(Player col)
		{
			return collidingWithPlayerTop(col) || collidingWithPlayerBottom(col) || collidingWithPlayerLeft(col) || collidingWithPlayerRight(col);
		}
	}
	
	class Projectile
	{
		float x, y, speed, size;
		boolean up, down, left, right, active;
		Color color = new Color(255, 225, 0, 255);
		DrawShape drawShape = new DrawShape();
		Collider2D collider;
		
		Projectile(float speed, float size)
		{
			this.speed = speed;
			this.size = size;
		}
		
		/**
		 * Setup the projectile's collider, color, and size
		 */
		public void setupProjectile()
		{
			collider = new Collider2D(x, y, size);
			color.fillColor();
			size = player.size/3;
		}
		
		/**
		 * Shoots a circle projectile
		 */
		public void fireCircleProjectile()
		{
			setupProjectile();
			addForce(speed);
			drawShape.circle(x, y, size);
			spawnOnHit();
		}
		
		/**
		 * Sets the projectile's direction to the player's direction
		 */
		public void setDirection()
		{
			if(player.facingUp)
			{
				up = true;
				down = false;
				left = false;
				right = false;
			}
			if(player.facingDown)
			{
				up = false;
				down = true;
				left = false;
				right = false;
			}
			if(player.facingLeft)
			{
				up = false;
				down = false;
				left = true;
				right = false;
			}
			if(player.facingRight)
			{
				up = false;
				down = false;
				left = false;
				right = true;
			}
		}
		
		/**
		 * Spawns a circle when the projectile hits the edge of the screen
		 */
		public void spawnOnHit()
		{
			if(collider.colliding() || collider.collidingWithPlayer(otherPlayer))
			{
				PShape circleClone = createShape(ELLIPSE, projectile.x, projectile.y, 100, 100);
				shape(circleClone);
				active = false;
			}
		}
		
		public void addForce(float speed)
		{
			if(up)
			{
				y-=speed;
			}
			if(down)
			{
				y+=speed;
			}
			if(left)
			{
				x-=speed;
			}
			if(right)
			{
				x+=speed;
			}
		}
	}
	
	class Player
	{
		// -- PROPERTIES --
		public Color color = new Color(0,255,0,255);
		float spawnX, spawnY, x, y, size, speed;
		boolean facingUp = true;
		boolean facingDown = false;
		boolean facingLeft = false;
		boolean facingRight = false;
		boolean selected = false;
		String text, name;
		Collider2D collider;
		
		// -- CONSTRUCTOR --
		/**
		 * Player constructor
		 * @param x position
		 * @param y position
		 * @param size scale
		 * @param speed move speed
		 */
		Player(float x, float y, float size, float speed)
		{
			this.x = x;
			this.y = y;
			this.size = size;
			this.speed = speed;
		}
		
		// -- METHODS --
		
		
		/**
		 * alpha --> 255
		 */
		public void show()
		{
			color.alpha = 255;
		}
		
		/**
		 * alpha --> 0
		 */
		public void hide()
		{
			color.alpha = 0;
		}
		
		/**
		 * <li>alpha --> 255
		 * <li>direction --> up
		 * <li>position --> center
		 */
		public void reset()
		{
			color.alpha = 255;
			faceUp();
			x = spawnX;
			y = spawnY;
		}
		
		/**
		 * Select player
		 */
		public void select()
		{
			player = this;
			selected = true;
			println(name + " selected");
		}
		
		/**
		 * Deselect player
		 */
		public void deselect()
		{
			otherPlayer = this;
			selected = false;
		}
		
		/**
		 * Move player to the top border
		 */
		public void ceiling()
		{
			faceUp();
			y = size/2;
			println(name + " moved to ceiling");
		}
		
		/**
		 * Move player to the bottom border
		 */
		public void floor()
		{
			faceDown();
			y = winY - size/2;
			println(name + " moved to floor");
		}

		/**
		 * Move player to the left border
		 */
		public void leftWall()
		{
			faceLeft();
			x = size/2;
			println(name + " moved to left wall");
		}
		
		/**
		 * Move player to the right border
		 */
		public void rightWall()
		{
			faceRight();
			x = winX -size/2;
			println(name + " moved to right wall");
		}
		
		/**
		 * <li>Face up
		 * <li>y - size
		 */
		public void moveUp()
		{
			faceUp();
			y -= speed;
			println(name + " moved up " + speed + " units");
		}
		
		/**
		 * <li>Face down
		 * <li>y + size
		 */
		public void moveDown()
		{
			faceDown();
			y += speed;
			println(name + " moved down " + speed + " units");
		}
		
		/**
		 * <li>Face left
		 * <li>x - size
		 */
		public void moveLeft()
		{
			faceLeft();
			x -= speed;
			println(name + " moved left " + speed + " units");
		}
		
		/**
		 * <li>Face right
		 * <li>x + size
		 */
		public void moveRight()
		{
			faceRight();
			x += speed;
			println(name + " moved right " + speed + " units");
		}
		
		/**
		 * <li><b>Facing Up</b>
		 * <li>Facing Down
		 * <li>Facing Left
		 * <li>Facing Right
		 */
		public void faceUp()
		{
			facingUp = true;
			facingDown = false;
			facingLeft = false;
			facingRight = false;
		}
		
		/**
		 * <li>Facing Up
		 * <li><b>Facing Down</b>
		 * <li>Facing Left
		 * <li>Facing Right
		 */
		public void faceDown()
		{
			facingUp = false;
			facingDown = true;
			facingLeft = false;
			facingRight = false;
		}
		
		/**
		 * <li>Facing Up
		 * <li>Facing Down
		 * <li><b>Facing Left</b>
		 * <li>Facing Right
		 */
		public void faceLeft()
		{
			facingUp = false;
			facingDown = false;
			facingLeft = true;
			facingRight = false;
		}
		
		/**
		 * <li>Facing Up
		 * <li>Facing Down
		 * <li>Facing Left
		 * <li><b>Facing Right</b>
		 */
		public void faceRight()
		{
			facingUp = false;
			facingDown = false;
			facingLeft = false;
			facingRight = true;
		}
		
		/**
		 * @return true: If the player's alpha is 255
		 */
		public boolean visible()
		{
			return color.alpha == 255;
		}
		
		/**
		 * Show text above the player
		 */
		public void showText(TrianglePlayer triangle, ImagePlayer doge)
		{
			if(player == triangle)
			{
				player.text = "Triangle selected";
			}
			else if(player == doge)
			{
				player.text = "Doge selected";
			}
			float textX = x - 3*text.length();
			float textY = y - player.size/2;
			text(text, textX, textY);
		}
		
		/**
		 * Set player position
		 * @param x position
		 * @param y position
		 */
		public void setPosition(float x, float y)
		{
			this.x = x;
			this.y = y;
		}
	}
	
	class TrianglePlayer extends Player
	{
		/**
		 * TrianglePlayer constructor
		 * @param x
		 * @param y
		 * @param size
		 * @param speed
		 */
		TrianglePlayer(float x, float y, float size, float speed) {
			super(x, y, size, speed);
		}

		/**
		 * Setup TrianglePlayer
		 */
		public void setup()
		{
			name = "TrianglePlayer";
			selected = true;
			println(name + " selected");
			spawnX = x;
			spawnY = y;
		}
		
		/**
		 * Spawn TrianglePlayer
		 */
		public void spawn()
		{
			float x1 = x;
			float y1 = y-size/2;
			float x2 = x-size/2;
			float y2 = y+size/2;
			float x3 = x+size/2;
			float y3 = y+size/2;
			selectColor();
			color.fillColor();
			if(visible())
			{
				stroke(20);
			}
			else
			{
				noStroke();	
			}
			if(facingUp)
			{
				triangle(x1, y1, x2, y2, x3, y3);
			}
			if(facingDown)
			{
				triangle(x1, y1+size, x2, y2-size, x3, y3-size);
			}
			if(facingLeft)
			{
				triangle(x1-size/2, y1+size/2, x2+size, y2, x3, y3-size);
			}
			if(facingRight)
			{
				triangle(x1+size/2, y1+size/2, x2, y2-size, x3-size, y3);
			}
		}
		
		/**
		 * Switch between green and dark green
		 */
		public void selectColor()
		{
			if(selected)
			{
				color.red = 0;
				color.green = 255;
				color.blue = 0;
			}
			else
			{
				color.red = 0;
				color.green = 200;
				color.blue = 0;
			}
		}
	}
	
	class ImagePlayer extends Player
	{
		// -- CONSTRUCTOR --
		/**
		 * ImagePlayer constructor
		 * @param x
		 * @param y
		 * @param size
		 * @param speed
		 */
		ImagePlayer(float x, float y, float size, float speed) {
			super(x, y, size, speed);
		}
		
		// -- PROPERTIES --
		PImage image;
		
		// -- METHODS --
		/**
		 * Load image
		 */
		public void setup()
		{
			name = "ImagePlayer";
			image = loadImage("doge.png");
			spawnX = x;
			spawnY = y;
		}
		
		/**
		 * Spawn image
		 */
		public void spawn()
		{ 	
			selectColor();
			image(image, x-size/2, y-size/2, size, size);
		}
		
		/**
		 * <li><b>Selected: </b> no tint
		 * <li><b>Not Selected: </b> dark tint
		 */
		public void selectColor()
		{
			if(selected)
			{
				tint(255);
			}
			else
			{
				tint(100);
			}
		}
		
	}
	
	class Color
	{
		// -- PROPERTIES --
		int red;
		int green;
		int blue;
		int alpha;
		int redDir = 1;
		int greenDir = 1;
		int blueDir = 1;
		
		// -- CONSTRUCTOR --
		/**
		 * 
		 * @param red
		 * @param green
		 * @param blue
		 * @param alpha
		 */
		Color(int red, int green, int blue, int alpha)
		{
			this.red = red;
			this.green = green;
			this.blue = blue;
			this.alpha = alpha;
		}
		
		// -- METHODS --
		/**
		 * Fill color
		 */
		public void fillColor()
		{
			fill(red, green, blue, alpha);
		}
		
		/**
		 * Changes color direction whenever it hits 255 or 0
		 */
		public void animateColor()
		{
			if(red > 255 || red < 0)
			{
				redDir *= -1;
			}
			if(green > 255 || green < 0)
			{
				greenDir *= -1;
			}
			if(blue > 255 || blue < 0)
			{
				blueDir *= -1;
			}
		}
		
		/**
		 * Animates the fill color
		 */
		public void animateFill()
		{
			animateColor();
			fill(red+=redDir, green+=greenDir, blue+=blueDir);
		}
		
		/**
		 * Animates the background alt color
		 */
		public void animateAltFill()
		{
			animateColor();
			fill((red+=redDir)-255+red, (green+=greenDir)-255+green, (blue+=blueDir)-255+blue);
		}
		
		/**
		 * Animates the background alt color
		 */
		public void animateAltBackground()
		{
			animateColor();
			background((red+=redDir)-255+red, (green+=greenDir)-255+green, (blue+=blueDir)-255+blue);
		}
	}
	
	class DrawShape
	{
		// -- METHODS --
		/**
		 * Draws a square
		 * @param x position
		 * @param y position
		 * @param size scale
		 */
		public void square(float x, float y, float size) {
			rect(x, y, size, size);
		}
		
		/**
		 * Draws a circle
		 * @param x position
		 * @param y position
		 * @param diameter
		 */
		public void circle(float x, float y, float diameter)
		{
			ellipse(x, y, diameter, diameter);
		}
	}
	
	class Shape
	{
		// -- PROPERTIES --
		float x, y, size, xDirection, yDirection, xSpeed, ySpeed;
		String name;
		DrawShape drawShape = new DrawShape();
		
		// -- CONSTRUCTOR --
		/**
		 * Shape Constructor
		 * @param xPos
		 * @param yPos
		 * @param shpSize
		 * @param dirX
		 * @param dirY
		 * @param spdX
		 * @param spdY
		 */
		Shape(float x, float y, float size, float xDirection, float yDirection, float xSpeed, float ySpeed)
		{
			this.x = x;
			this.y = y;
			this.size = size;
			this.xDirection = xDirection;
			this.yDirection = yDirection;
			this.xSpeed = xSpeed;
			this.ySpeed = ySpeed;
		}
		
		// -- METHODS --
		
		/**
		 * Moves the shape by adding (direction * speed)
		 */
		public void move()
		{
			x += (xDirection * xSpeed);
			y += (yDirection * ySpeed);
		}
		
		public void directionUp()
		{
			xDirection = 0;
			yDirection = -1;
		}
		public void directionDown()
		{
			xDirection = 0;
			yDirection = 1;
		}
		public void directionLeft()
		{
			xDirection = -1;
			yDirection = 0;
		}
		public void directionRight()
		{
			xDirection = 1;
			yDirection = 0;
		}
		
		// -- BOOLEANS --
		/**
		 * Hit right of the screen - size * (index + 1)
		 * @param i 
		 * @return x > winX - size*(i+1)
		 */
		public boolean hitRight(int i)
		{
			return x > winX - size*(i+1);
		}
		/**
		 * Hit bottom of the screen - size * (index + 1)
		 * @param i 
		 * @return y > winY - size*(i+1)
		 */
		public boolean hitBottom(int i)
		{
			return y > winY - size*(i+1);
		}
		/**
		 * Hit left of the screen + size * index
		 * @param i 
		 * @return x < size*(i)
		 */
		public boolean hitLeft(int i)
		{
			return x < size*(i);
		}
		/**
		 * Hit top of the screen + size * index
		 * @param i 
		 * @return y < size*(i)
		 */
		public boolean hitTop(int i)
		{
			return y < size*(i);
		}
	}
	
	class Ellipse extends Shape
	{
		// -- CONSTRUCTOR --
		/**
		 * Ellipse constructor
		 * @param x
		 * @param y
		 * @param size
		 * @param xDirection
		 * @param yDirection
		 * @param xSpeed
		 * @param ySpeed
		 */
		Ellipse(float x, float y, float size, float xDirection, float yDirection, float xSpeed, float ySpeed) {
			super(x, y, size, xDirection, yDirection, xSpeed, ySpeed);
		}
		
		// -- PROPERTIES --
		
		ArrayList<Ellipse> list;
		float radius = size/2;
		
		// -- METHODS --
		
		public void drawCircle()
		{
			drawShape.circle(x, y, size);
		}
		
		/**
		 * Ellipse bounces off the edge of the window
		 */
		public void bounce()
		{
			if(x > width - radius || x < radius)
			{
				xDirection *= -1;
			}
			if(y > height - radius || y < radius)
			{
				yDirection *= -1;
			}
		}
		
		/**
		 * Adds circles to ellipse ArrayList
		 * @param list List to store ellipses
		 * @param numCircles Number of circles to store into the list
		 * @param c The circle that will be referenced for cloning
		 */
		public void setupCircles(ArrayList<Ellipse> arrayList, int numCircles)
		{
			list = arrayList;
			for(int i=1;i<numCircles+1;i++)
			{
				Ellipse circle = new Ellipse(x+i*50, y+i*50, size, xDirection, yDirection, xSpeed, ySpeed);
				//new Ellipse(i*50, i*50, 50, -1, 1, 5, 5)
				arrayList.add(circle);
			}
		}
		
		/**
		 * Move all circles in list
		 * @param list Ellipse ArrayList which contains the circles that will be moved
		 */
		public void moveCircles()
		{
			for(int i=0;i<list.size();i++)
			{
				Ellipse c = list.get(i);
				c.drawCircle();
				c.move();
				c.bounce();
			}
		}
		
		/**
		 * Loop the ellipse around the window
		 */
		public void squareLoop()
		{
			for(int i=0;i<list.size();i++)
			{
				Ellipse c = list.get(i);
				c.drawCircle();
				
				// top left --> top right
				if(c.hitRight(i))
				{
					c.directionDown();
				}
				
				// top right --> bottom right
				if(c.hitBottom(i))
				{
					c.directionLeft();
				}
				
				// bottom right --> bottom left
				if(c.hitLeft(i))
				{
					c.x += c.xSpeed;
					c.directionUp();
				}
				
				// bottom left --> top left
				if(c.hitTop(i))
				{
					c.y += c.ySpeed;
					c.directionRight();
				}
				c.move();
			}
		}
	}
	
	class Rectangle extends Shape
	{
		// -- CONSTRUCTOR --
		/**
		 * Rectangle constructor
		 * @param x
		 * @param y
		 * @param size
		 * @param xDirection
		 * @param yDirection
		 * @param xSpeed
		 * @param ySpeed
		 */
		Rectangle(float x, float y, float size, float xDirection, float yDirection, float xSpeed, float ySpeed) {
			super(x, y, size, xDirection, yDirection, xSpeed, ySpeed);
		}
		
		// -- PROPERTIES --

		ArrayList<Rectangle> list;
		
		// -- METHODS --
		
		public void drawSquare()
		{
			drawShape.square(x, y, size);
		}
		
		/**
		 * Rectangle bounces off the edge of the window
		 */
		public void bounce()
		{
			if(x > winX - size || x < 0)
			{
				xDirection *= -1;
			}
			if(y > winY - size || y < 0)
			{
				yDirection *= -1;
			}
		}
		
		/**
		 * Adds squares to rectangle ArrayList
		 * @param list List to store rectangles
		 * @param numSquares Number of squares to store into the list
		 * @param r The square that will be referenced for cloning
		 */
		public void setupSquares(ArrayList<Rectangle> arrayList, int numSquares)
		{
			list = arrayList;
			for(int i=1;i<numSquares+1;i++)
			{
				Rectangle square = new Rectangle(x+i*50, y+i*50, size, xDirection, yDirection, xSpeed, ySpeed);
				arrayList.add(square);
			}
		}
		/**
		 * Move all squares in list
		 * @param list Rectangle ArrayList which contains the squares that will be moved
		 */
		public void moveSquares()
		{
			for(int i=0;i<list.size();i++)
			{
				Rectangle s = list.get(i);
				s.drawSquare();
				s.move();
				s.bounce();
			}
		}
		
		/**
		 * Loop the ellipse around the window
		 */
		public void squareLoop()
		{
			for(int i=0;i<list.size();i++)
			{
				Rectangle s = list.get(i);
				s.drawSquare();
				
				// top left --> top right
				if(s.hitRight(i))
				{
					s.directionDown();
					s.x -= s.xSpeed;
				}
				
				// top right --> bottom right
				if(s.hitBottom(i))
				{
					s.directionLeft();
					s.y -= s.ySpeed;
				}
				
				// bottom right --> bottom left
				if(s.hitLeft(i))
				{
					s.directionUp();
					s.x += s.xSpeed;
				}
				
				// bottom left --> top left
				if(s.hitTop(i))
				{
					s.directionRight();
					s.y += s.ySpeed;
				}
				s.move();
			}
		}
	}
}
